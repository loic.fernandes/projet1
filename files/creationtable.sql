
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `utilisateurs` (
  `IDuser` int(11) NOT NULL,
  `Login` varchar(30) NOT NULL,
  `Dateinscription` date NOT NULL,
  `Dateresiliation` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`IDuser`);

